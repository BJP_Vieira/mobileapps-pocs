﻿using System;
using System.Collections.Generic;
using Xamarin.Forms;

namespace Controls
{
    public partial class SectionHeader : TemplatedView
    {
        public SectionHeader()
        {
            InitializeComponent();
        }

        public string HeaderText
        {
            get { return (string)GetValue(HeaderTextProperty); }
            set { SetValue(HeaderTextProperty, value); }
        }

        // Using a DependencyProperty as the backing store for HeaderText.  This enables animation, styling, binding, etc...
        public static readonly BindableProperty HeaderTextProperty =
            BindableProperty.Create(nameof(HeaderText), typeof(string), typeof(SectionHeader), default(string));

    }
}
