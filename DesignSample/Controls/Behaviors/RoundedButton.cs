﻿using System;
using System.Linq;
using Xamarin.Forms;

namespace Controls.Behaviors
{
    public class RoundedButton : Behavior<Button>
    {
        protected override void OnAttachedTo(Button bindable)
        {
            bindable.SizeChanged += Frame_SizeChanged;
        }

        private void Frame_SizeChanged(object sender, EventArgs e)
        {
            var b = new Button();
            b.CornerRadius = 2;
            ((Button)sender).CornerRadius = (int)(((Button)sender).Height / 2);
        }

        protected override void OnDetachingFrom(Button bindable)
        {
            bindable.SizeChanged -= Frame_SizeChanged;
        }

        public static readonly BindableProperty AttachBehaviorProperty =
        BindableProperty.CreateAttached("AttachBehavior", typeof(bool), typeof(RoundedButton), false, propertyChanged: OnAttachBehaviorChanged);

        public static bool GetAttachBehavior(BindableObject view)
        {
            return (bool)view.GetValue(AttachBehaviorProperty);
        }

        public static void SetAttachBehavior(BindableObject view, bool value)
        {
            view.SetValue(AttachBehaviorProperty, value);
        }

        static void OnAttachBehaviorChanged(BindableObject view, object oldValue, object newValue)
        {
            var entry = view as Button;
            if (entry == null)
            {
                return;
            }

            bool attachBehavior = (bool)newValue;
            if (attachBehavior)
            {
                entry.Behaviors.Add(new RoundedButton());
            }
            else
            {
                var toRemove = entry.Behaviors.FirstOrDefault(b => b is RoundedButton);
                if (toRemove != null)
                {
                    entry.Behaviors.Remove(toRemove);
                }
            }
        }
    }
}
